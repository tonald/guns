package com.stylefeng.guns.modular.orderManager.service.impl;

import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.stylefeng.guns.modular.orderManager.dao.OrderMapper;
import com.stylefeng.guns.modular.orderManager.model.Order;
import com.stylefeng.guns.modular.orderManager.service.IOrderService;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author stylefeng
 * @since 2018-10-30
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {
	 @Override
	    public List<Map<String, Object>> selectOrders(String name) {
	        return this.baseMapper.selectOrders(name);
	    }
}
