package com.stylefeng.guns.modular.orderManager.dao;

import com.stylefeng.guns.core.datascope.DataScope;
import com.stylefeng.guns.modular.orderManager.model.Order;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.annotations.Param;

import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 订单表 Mapper 接口
 * </p>
 *
 * @author stylefeng
 * @since 2018-10-30
 */
public interface OrderMapper extends BaseMapper<Order> {

    /**
     * 根据条件查询订单列表
     */
    List<Map<String, Object>> selectOrders(@Param("name") String name);

}
